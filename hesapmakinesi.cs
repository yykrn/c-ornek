using System;
using System.Collections.Generic;

public class Program
{
	public static void Main()
	{
		string input = Console.ReadLine();
		List<string> operators = new List<string>();
		operators.Add("^");
		operators.Add("*");
		operators.Add("/");
		operators.Add("+");
		operators.Add("-");
		Dictionary<string, Func<double, double, double>> dic = new Dictionary<string, Func<double, double, double>>{{"+", (x, y) => x + y}, {"-", (x, y) => x - y}, {"*", (x, y) => x * y}, {"/", (x, y) => x / y}, {"^", (x, y) => Math.Pow(x,y)}};
		List<string> operation = new List<string>();
		string str = "";
		for (int i = 0; i < input.Length; i++)
		{
			if (char.IsDigit(input[i]))
			{
				str += input[i];
			}
			else
			{
				string temp = string.Copy(str);
				operation.Add(temp);
				operation.Add(input[i].ToString());
				str = "";
			}
		}

		operation.Add(string.Copy(str));
		
		foreach (string s in operators)
		{
			while (true)
			{
				int index = -1;
				index = operation.FindIndex(x => x.Equals(s));
				if (index == -1)
					break;
				double result = dic[s](double.Parse(operation[index - 1]), double.Parse(operation[index + 1]));
				operation[index - 1] = result.ToString();
				operation.RemoveAt(index+1);
				operation.RemoveAt(index);
			}
		}
		Console.WriteLine("{0}",operation[0]);
	}
}